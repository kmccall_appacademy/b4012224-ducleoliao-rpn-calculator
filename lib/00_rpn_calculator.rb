class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map {|ch| operation?(ch) ? ch.to_sym : Integer(ch) }
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    self.value

  end

private

  def operation?(operation)
    [:+, :-, :*, :/].include?(operation.to_sym)
  end

  def perform_operation(operation)
    raise "calculator is empty" if @stack.size < 2
    second_operand = @stack.pop
    first_operand = @stack.pop
    case operation
    when :+
      @stack << first_operand + second_operand
    when :-
      @stack << first_operand - second_operand
    when :*
      @stack << first_operand * second_operand
    when :/
      @stack << first_operand.fdiv(second_operand)
    else
      @stack << first_operand
      @stack << second_operand
      raise "There is no such operation: #{operation}"
    end
  end
end
